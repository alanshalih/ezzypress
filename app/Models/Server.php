<?php

namespace App\Models;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    use HasFactory;

    protected $guarded = [];

    

    public static function boot()
    {
        parent::boot();

        self::saving(function($server){
            // ... code here
         

             $server->remains = $server->limit - $server->usage;
            
        });

    }
}
