<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class InstallWordpress implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $timeout = 30;

    public $tries = 5;
    
    public $backoff = 5;

    
    protected $web;

    public function __construct($web)
    {
        //
        $this->web = $web;

        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        $web = $this->web;
        

        
        $client = new Client();

        $server = $this->web->server;
      

        // install wp dari ploi 

        $client->request('POST', "https://ploi.io/api/servers/{$server->server_id}/sites/{$this->web->webapp_id}/wordpress", [
            'json'    => ["create_database"=>true],
            'headers' => 
                [
                    'Accept'        => 'application/json',
                    'Authorization' => "Bearer {$server->api_key}"
                ]
            
        ]);

        InstallWeb::dispatch($this->web)->delay(now()->addSeconds(10)); 


         
    }
}
