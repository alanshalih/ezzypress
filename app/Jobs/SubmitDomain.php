<?php

namespace App\Jobs;

use App\Models\MainDomain;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class SubmitDomain implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $timeout = 30;

    public $tries = 5;
    
    protected $web;
    public function __construct($web)
    {
        //
        $this->web = $web;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Redis::throttle('cloudflare-api-limiter')->block(0)->allow(1200)->every(300)->then(function () {
            
            info('submit domain...'); 

            $client = new Client();

            $cloudflare = MainDomain::where('name',$this->web->tld)->first();

            $res = $client->request('POST', "https://api.cloudflare.com/client/v4/zones/{$cloudflare->zone_id}/dns_records", [
                'json'    => ["type"=>"CNAME","name"=>$this->web->reserve_domain,"content"=>$this->web->server->domain,"proxied"=>true],
                'headers' => [
                    'X-Auth-Email' => $cloudflare->cloudflare_email,
                    'X-Auth-Key' => $cloudflare->cloudflare_auth,
                ],
            ]);

            $data =  json_decode($res->getBody()); 

            $this->web->cloudflare_dns_id = $data->result->id;

            $this->web->http_protocol = 'http';

            $this->web->save(); 


            
            // Handle job...
        }, function () {
            // Could not obtain lock...
    
            return $this->release(60);
        });
    }
}
