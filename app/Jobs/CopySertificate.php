<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CopySertificate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $timeout = 30;

    public $tries = 5;
    
    public $backoff = 5;

    
    protected $web;

    public function __construct($web)
    {
        //
        $this->web = $web;

        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        $web = $this->web;
         

        
        $client = new Client();
 

        $client->request('POST', "https://".$web->server_url."/api/copy-certificate", [
            'json'    => $web
        ]);
 
        ForceHTTPs::dispatch($this->web);

         
    }
}
