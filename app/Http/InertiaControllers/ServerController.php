<?php

namespace App\Http\InertiaControllers;

use App\Models\Server;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return Inertia::render('Servers/Index', [
                'servers' => Server::paginate(10),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create(): \Inertia\Response
    {
        return Inertia::render('Servers/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Redirect
     */
    public function store(Request $request): Redirect
    {
        return Redirect::route('servers')->with('success', 'Server created.');
    }

    /**
     * Display the specified resource.
     *
     * @param Server $server
     * @return \Inertia\Response
     */
    public function show(Server $server): \Inertia\Response
    {
        return Inertia::render('Servers/Show', [
                'server' => $server
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Server $server
     * @return \Inertia\Response
     */
    public function edit(Server $server): \Inertia\Response
    {
        return Inertia::render('Servers/Edit', [
                'server' => $server
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Server $server
     * @return Redirect
     */
    public function update(Request $request, Server $server): Redirect
    {
        return Redirect::back()->with('success', 'Server updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Server $server
     * @return Redirect
     */
    public function destroy(Server $server): Redirect
    {
        return Redirect::back()->with('success', 'Server restored.');
    }

    /**
     * Restore the specified resource.
     *
     * @param Server $server
     * @return Redirect
     */
    public function restore(Server $server): Redirect
    {
        $server->restore();

        return Redirect::back()->with('success', 'Server restored.');
    }
}
