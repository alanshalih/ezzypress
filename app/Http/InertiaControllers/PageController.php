<?php

namespace App\Http\InertiaControllers;

use App\Models\Page;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return Inertia::render('Pages/Index', [
                'pages' => Page::paginate(10),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create(): \Inertia\Response
    {
        return Inertia::render('Pages/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Redirect
     */
    public function store(Request $request): Redirect
    {
        return Redirect::route('pages')->with('success', 'Page created.');
    }

    /**
     * Display the specified resource.
     *
     * @param Page $page
     * @return \Inertia\Response
     */
    public function show(Page $page): \Inertia\Response
    {
        return Inertia::render('Pages/Show', [
                'page' => $page
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Page $page
     * @return \Inertia\Response
     */
    public function edit(Page $page): \Inertia\Response
    {
        return Inertia::render('Pages/Edit', [
                'page' => $page
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Page $page
     * @return Redirect
     */
    public function update(Request $request, Page $page): Redirect
    {
        return Redirect::back()->with('success', 'Page updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Page $page
     * @return Redirect
     */
    public function destroy(Page $page): Redirect
    {
        return Redirect::back()->with('success', 'Page restored.');
    }

    /**
     * Restore the specified resource.
     *
     * @param Page $page
     * @return Redirect
     */
    public function restore(Page $page): Redirect
    {
        $page->restore();

        return Redirect::back()->with('success', 'Page restored.');
    }
}
