<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        
        self::saving(function($plan){
            // ... code here

            if(!$plan->plan_id)
            {
                $plan->plan_id = md5(now().rand(0,11111111));
            }
             if($plan->time_unit == 'Bulan')
             {
                $plan->time_in_day = $plan->time * 30;
                $plan->time_in_month = $plan->time * 1; 
                $plan->daily_price = ceil($plan->price / $plan->time_in_day);
                $plan->monthly_price = ceil($plan->price / $plan->time_in_month);
             }if($plan->time_unit == 'Hari')
             {
                $plan->time_in_day = $plan->time;
                $plan->time_in_month = $plan->time * 0; 
                $plan->daily_price = ceil($plan->price / $plan->time_in_day);
                $plan->monthly_price = ceil($plan->price / $plan->time_in_month);
             }else{
                $plan->time_in_day = $plan->time * 365;
                $plan->time_in_month = $plan->time * 12; 
                $plan->daily_price = ceil($plan->price / $plan->time_in_day);
                $plan->monthly_price = ceil($plan->price / $plan->time_in_month);
             } 

             $plan->point_includes = $plan->point_includes ? json_encode($plan->point_includes) : json_encode([]);
           

             if($plan->default_plan)
             {
                 Plan::where('default_plan',true)->update(["default_plan"=>false]); 
             }
        });

        self::retrieved(function($plan){
            $plan->point_includes = json_decode($plan->point_includes);
        });

    }

}
