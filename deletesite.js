var sites = [
    {
        "id": 56626,
        "status": "active",
        "server_id": 22542,
        "domain": "webmuda.maxgrabb.com",
        "test_domain": null,
        "deploy_script": false,
        "web_directory": "/public",
        "project_type": null,
        "project_root": "/",
        "last_deploy_at": null,
        "system_user": "ploi",
        "php_version": "8.0",
        "health_url": null,
        "has_repository": false,
        "zero_downtime_deployment": false,
        "fastcgi_cache": false,
        "created_at": "2021-06-25 14:35:51"
    },
    {
        "id": 56627,
        "status": "active",
        "server_id": 22542,
        "domain": "webtua.maxgrabb.com",
        "test_domain": null,
        "deploy_script": false,
        "web_directory": "/public",
        "project_type": "wordpress",
        "project_root": "/",
        "last_deploy_at": null,
        "system_user": "ploi",
        "php_version": "8.0",
        "health_url": null,
        "has_repository": false,
        "zero_downtime_deployment": false,
        "fastcgi_cache": false,
        "created_at": "2021-06-25 14:36:49"
    },
    {
        "id": 56630,
        "status": "active",
        "server_id": 22542,
        "domain": "tiga.maxgrabb.com",
        "test_domain": null,
        "deploy_script": false,
        "web_directory": "/public",
        "project_type": null,
        "project_root": "/",
        "last_deploy_at": null,
        "system_user": "ploi",
        "php_version": "8.0",
        "health_url": null,
        "has_repository": false,
        "zero_downtime_deployment": false,
        "fastcgi_cache": false,
        "created_at": "2021-06-25 14:42:46"
    },
    {
        "id": 56631,
        "status": "active",
        "server_id": 22542,
        "domain": "menunda.maxgrabb.com",
        "test_domain": null,
        "deploy_script": false,
        "web_directory": "/public",
        "project_type": null,
        "project_root": "/",
        "last_deploy_at": null,
        "system_user": "ploi",
        "php_version": "8.0",
        "health_url": null,
        "has_repository": false,
        "zero_downtime_deployment": false,
        "fastcgi_cache": false,
        "created_at": "2021-06-25 14:46:45"
    }]
var axios  = require('axios')
sites.forEach(item=>{
    axios.delete('https://ploi.io/api/servers/22542/sites/'+item.id,{headers : {
        Authorization : "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZjM1Yzc1NDY5ZmIxMjNjODE2MmYyZDM1NDcwZGI5MDAwZjI1MDJjMzFmNjEwMjVlOWJlZTBlN2Q5OGUyY2M0NGM2NGUxNGY4ZGQ3YjdjYTMiLCJpYXQiOjE2MjM2NTQzMzkuNzgxNzE0LCJuYmYiOjE2MjM2NTQzMzkuNzgxNzE2LCJleHAiOjE2ODY3MjYzMzkuNzY4NDA5LCJzdWIiOiI5MTY5Iiwic2NvcGVzIjpbInNlcnZlcnMtcmVhZCIsInNlcnZlcnMtY3JlYXRlIiwic2VydmVycy1kZWxldGUiLCJkYXRhYmFzZS1yZWFkIiwiZGF0YWJhc2UtY3JlYXRlIiwiZGF0YWJhc2UtZGVsZXRlIiwiZGFlbW9ucy1yZWFkIiwiZGFlbW9ucy1jcmVhdGUiLCJkYWVtb25zLWRlbGV0ZSIsImNyb25qb2JzLXJlYWQiLCJjcm9uam9icy1jcmVhdGUiLCJjcm9uam9icy1kZWxldGUiLCJuZXR3b3JrLXJ1bGVzLXJlYWQiLCJuZXR3b3JrLXJ1bGVzLWNyZWF0ZSIsIm5ldHdvcmstcnVsZXMtZGVsZXRlIiwic3lzdGVtLXVzZXJzLXJlYWQiLCJzeXN0ZW0tdXNlcnMtY3JlYXRlIiwic3lzdGVtLXVzZXJzLWRlbGV0ZSIsInNzaC1rZXlzLXJlYWQiLCJzc2gta2V5cy1jcmVhdGUiLCJzc2gta2V5cy1kZWxldGUiLCJzaXRlcy1yZWFkIiwic2l0ZXMtY3JlYXRlIiwic2l0ZXMtZGVsZXRlIiwicmVkaXJlY3RzLXJlYWQiLCJyZWRpcmVjdHMtY3JlYXRlIiwicmVkaXJlY3RzLWRlbGV0ZSIsImNlcnRpZmljYXRlcy1yZWFkIiwiY2VydGlmaWNhdGVzLWNyZWF0ZSIsImNlcnRpZmljYXRlcy1kZWxldGUiLCJhdXRoLXVzZXJzLXJlYWQiLCJhdXRoLXVzZXJzLWNyZWF0ZSIsImF1dGgtdXNlcnMtZGVsZXRlIiwiYWxpYXNlcy1yZWFkIiwiYWxpYXNlcy1jcmVhdGUiLCJhbGlhc2VzLWRlbGV0ZSJdfQ.sN8yYH-zrY8__KmDMSf1WGSg10s1lKZZx5Q3_xHTTKco0HEe7-6UqRBU5Sk5wQeumhjRxhJJBHx3EIFOh0eGUWNsRlCxr5wF9NQ0EJoOPWPJzIP37G-xmS5I13ZJnK9OpGK99PwtIUcHo3I9ujd9DlikpsHLEhlaMgO8lyk6WxSG85YNbRJvZLNoXUcLyAeH0mku-GIHMDWbeXq43QIYx5aVBwkab2fMildsqCv4RSYk5VSaa6qdkJWuUKGHSXwSGYbddGtQW62e5x1BY9axZvAYrMf3qqmzmOk0k4fgackp4j0vprCdTQ47JEyfsjoZoIY9UTQEptfJWzssYtT_B_aB1wAT6uc4kYFDS3MWpyOfscI5MHIb843uUU1_B2ZqfY-60BEnlOWPN9578yLnVX7TsxlVOnFiFPI8_tMRBaHHL25Zfyv--9SVJRAtaV1-IHbhdNtFo0zFOIRmq58qxZ3eoavQ6hdv4hUkacaydW9Xc21sWmvMraXqGztkx3LUQ_yG9wac-HGrKBxkXvk3EkLA6dS--FeevJwaSoWKW49XgqLKs97u29ghvBmDQ--4RPlLb1klRan2VwjKtTQo22X3IcIMmHp3-liWbEBNOGX7U_qdSmw07AOxfFIxmXgcFCE6Dr1NPPAjHfAyFDXbx57L7Qk7Wj0sqBv-suHRvas"
    }})
})