<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetDBConfig implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $web;
    
    public function __construct($web)
    {
        //
        $this->web = $web;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        $client = new Client();

        $server = $this->web->server;
      

        info('install db');


        $config = $client->request('POST', "https://".$server->server_url."/api/get-wp-config", [
            'json'    => $this->web
        ]);

        $data =  json_decode($config->getBody()); 
        
        $this->web->db_name = $data[0]->value;

        $this->web->db_username = $data[1]->value;

        $this->web->db_password = $data[2]->value;

        $this->web->save();

        

    }
}
