<?php

use App\Events\WebsiteInstallationStatus;
use App\Http\Controllers\MainDomainController;
use App\Http\Controllers\MembershipController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\ServerController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WebsiteController;
use App\Jobs\CreateDatabase;
use App\Jobs\CreateSystemUser;
use App\Jobs\SubmitDomain;
use App\Models\Server;
use App\Models\User;
use App\Models\Website;
use GuzzleHttp\Client;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});


Route::get('/https/{id}', function ($id) {
    $web = Website::find($id);
     
    $client = new Client();

    $client->request('GET', "https://".$web->main_domain);
    
    return 'OK';
});

  

Route::middleware(['auth:sanctum', 'verified'])->resource('/website', WebsiteController::class);

Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    Route::get('/server/test',function(){
        $server = Server::first();

        
        $client = new Client();
 
 
 
        

        $ploi = $client->request('POST', "https://ploi.io/api/servers/{$server->server_id}/system-users", [
            'json'    => ["name"=>"mimpi","sudo"=>false],
            ['headers' => 
                [
                    'Authorization' => "Bearer {$server->api_key}"
                ]
            ]
        ]);

        return $ploi->getBody();


    });
    
    Route::get('/dashboard', [WebsiteController::class, 'index'])->name('dashboard');;
    Route::get('/membership', [MembershipController::class, 'index'])->name('membership');
    Route::get('/checkout', [MembershipController::class, 'create']);
    Route::get('/checkout/{id}', [MembershipController::class, 'show']);
    Route::post('/sale', [SaleController::class, 'store']);
    Route::get('/invoice/{id}',[SaleController::class, 'show']);
 
    Route::post('/website/{id}/open-pass', [WebsiteController::class, 'openPass'])->middleware(['password.confirm']);
    Route::post('/website/{id}/system-user-pass', [WebsiteController::class, 'ShowSystemUser'])->middleware(['password.confirm']);
    Route::post('/website/{id}/reset-system-user-pass', [WebsiteController::class, 'ResetSystemUser'])->middleware(['password.confirm']);
    
    // Route::post('/website/{id}/install/1', [WebsiteController::class, 'step1']);
    // Route::post('/website/{id}/install/2', [WebsiteController::class, 'step2']);
    // Route::post('/website/{id}/install/3', [WebsiteController::class, 'step3']);
    Route::post('/website/{id}/change-domain', [WebsiteController::class, 'changeDomain']);
    Route::post('/website/{id}/change-existed-domain', [WebsiteController::class, 'changeExistedDomain']);
    


    
    Route::middleware(['admin'])->group(function () {
        
        Route::get('/menu-admin', function(Request $request){
             return Inertia::render('MenuAdmin');
        });
        Route::post('/menu-admin', function(Request $request){
            Redis::set('appName',$request->appName);
            Redis::set('tutorLink',$request->tutorLink);
            Redis::set('docLink',$request->docLink);
            Redis::set('caraPembayaran',$request->caraPembayaran);
            return redirect('/menu-admin');
        })->name('menu-admin');
        Route::get('/plans', [PlanController::class, 'index'])->name('plans');
        Route::get('/plans/create', [PlanController::class, 'create']);
        Route::put('/plans/{id}', [PlanController::class, 'update']);
        Route::post('/sale/{id}/confirm', [SaleController::class, 'confirm']);
        Route::get('/sales', [SaleController::class, 'index']);
        Route::post('/plans', [PlanController::class, 'store']);
        Route::get('/plans/{id}/edit', [PlanController::class, 'edit']);
        Route::resource('/server',ServerController::class);
        Route::resource('/domain',MainDomainController::class);
        Route::resource('/users',UserController::class);
    });


});