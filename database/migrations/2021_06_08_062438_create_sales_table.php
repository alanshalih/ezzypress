<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_id')->index();
            $table->integer('price');
            $table->integer('discount')->default(0);
            $table->string("type")->default("hosting");
            $table->integer('plan_id')->nullable();
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('affiliate_id')->nullable()->constrained('users');
            $table->integer('affiliate_fee')->default(0);
            $table->string('title');
            $table->string('coupoun')->nullable();
            $table->integer('month_number');
            $table->string('status')->default('belum dibayar')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
