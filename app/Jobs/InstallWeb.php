<?php

namespace App\Jobs;

use App\Events\WebsiteInstallationStatus;
use App\Models\Website;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class InstallWeb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $web;

    public $timeout = 40;

    public $tries = 8;

    public $backoff = 10;

    // public $backoff = 60;
    // mengulang kembali setelah 60

    public function __construct($web)
    {
        // 
        $this->web = $web;;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $server = $this->web->server;

        $client = new Client();

      

        //         https://dua.ezzypress.online/wp-admin/install.php?step=2

        // weblog_title: JAKARTA
        // user_name: admin
        // admin_password: !*1hZfQ8F$X5UYZ&(1
        // admin_password2: !*1hZfQ8F$X5UYZ&(1
        // admin_email: maulanashalihin@gmail.com
        // Submit: Instal WordPress
        // language: id_ID
        $web = $this->web;

        $web->makeVisible(['website_password','db_password']);

        

        // $client->request('POST', "https://".$server->domain."/api/install-wordpress", [
        //     'json'    => $web
        // ]); 

        $client->request('GET', "http://{$web->main_domain}/wp-admin/install.php?step=1"); 
      
 
        $response = $client->post(
            'http://'.$web->main_domain.'/wp-admin/install.php?step=2',
            array(
                'form_params' => array(
                    'weblog_title' => $web->website_title,
                    'user_name' => $web->website_username,
                    'admin_password' => $web->website_password,
                    'admin_password2' => $web->website_password,
                    'admin_email' => $web->website_email,
                    'Submit' => 'Instal WordPres',
                    'language' => 'id_ID'
                )
            )
        );
 

      

        $this->web->installed = true;

        $this->web->save();

        
        WebsiteInstallationStatus::dispatch($this->web->only(["installed","step","step_status","id","user_id","http_protocol"]));

        CopySertificate::dispatch($this->web); 

        GetDBConfig::dispatch($this->web)->delay(now()->addSeconds(15)); 

        EnableFastCGI::dispatch($this->web)->delay(now()->addSeconds(15)); 



        // EnableCloudflareProxy::dispatch($this->web)->delay(now()->addSeconds(10)); 

        // InstallCloudflarePlugin::dispatch($this->web)->delay(now()->addSeconds(10)); 

    }

    // public function retryUntil(){
    //     // coba terus sampai 3 menit berlalu. baru declare gagal.
    //     return now()->addMinutes(3);
    // }
}
