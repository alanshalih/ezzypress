<?php

namespace Database\Seeders;

use App\Models\Plan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Plan::create(["title"=>"Starter Plan",
        "description"=>"paket pemula untuk memulai buat website",
                        "available_in_checkout"=>false,
                        "active_plan"=>true,
                        "is_renewable"=>false,
                        "time"=>1,
                        "time_in_day"=>30,
                        "time_in_month"=>1,
                        "time_unit"=>"Bulan",
                        "website_number"=>1,
                        "max_monthly_traffic"=>10000,
                        "max_hourly_traffic"=>1000,
                        "max_monthly_transfer" => 100000000,
                        "max_hourly_transfer" => 10000000,
                        "price"=>0,
                        "fake_price"=>30000,
                        "daily_price"=>0,
                        "monthly_price"=>0,
                        "default_plan"=>true]);
    }
}
