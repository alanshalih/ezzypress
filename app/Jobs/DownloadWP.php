<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class DownloadWP implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $timeout = 30;

    public $tries = 5;

    public $backoff = 10;

    
    protected $web;

    public function __construct($web)
    {
        //
        $this->web = $web;

        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        $web = $this->web;
        
        $web->makeVisible(['website_password','db_password']);

        
        info('download wp');

        $client = new Client();

        $server = $this->web->server;
      
        // $client->request('GET', "https://".$web->reserve_domain);


        $client->request('POST', "https://".$server->domain."/api/download-wordpress", [
            'json'    => $web
        ]);

        ConfigWordpress::dispatch($this->web)->delay(now()->addSeconds(2));;;


         
    }
}
