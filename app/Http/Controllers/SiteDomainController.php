<?php

namespace App\Http\Controllers;

use App\Models\SiteDomain;
use Illuminate\Http\Request;

class SiteDomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiteDomain  $siteDomain
     * @return \Illuminate\Http\Response
     */
    public function show(SiteDomain $siteDomain)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteDomain  $siteDomain
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteDomain $siteDomain)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SiteDomain  $siteDomain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteDomain $siteDomain)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiteDomain  $siteDomain
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteDomain $siteDomain)
    {
        //
    }
}
