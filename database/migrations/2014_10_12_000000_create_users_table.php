<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->string('profile_photo_path', 2048)->nullable(); 
            $table->string('system_user')->nullable();
            $table->string('system_user_pass')->nullable(); 
            $table->integer('system_user_id')->nullable();  
            $table->integer("plan_id")->default(1);
            $table->dateTime("membership_time");
            $table->boolean('is_admin')->default(false);
            $table->integer('penalized')->default(0);
            $table->boolean('suspended')->default(false); 
            $table->dateTime("system_user_password_expired_at")->nullable();
            $table->bigInteger('server_id')->unsigned()->nullable();
            $table->foreign('server_id')->references('id')->on('servers') ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
