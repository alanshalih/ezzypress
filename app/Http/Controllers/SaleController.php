<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use App\Models\Sale;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Inertia\Inertia;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sales = Sale::with('user')->paginate(100);
        return Inertia::render('Membership/Sales',["sales"=>$sales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $plan = Plan::find($request->plan);
        
        $sale = Sale::create(["invoice_id"=>strtoupper(substr(md5(auth()->user()->id.'-'.rand(0,100000).date("Y-m-d")),0,10)),"price"=>$plan->price,"plan_id"=>$plan->id,"user_id"=>auth()->user()->id,"title"=>$plan->title.' '.$plan->time.' '.$plan->time_unit,"month_number"=>$plan->time_in_month]);
        
        if($sale->price == 0)
        {
            $sale->status = 'berhasil';
            
            $sale->save();

            $now = Carbon::now();

            $user = User::find($sale->user_id);

            $user->membership_time = $now->addDays($plan->time_in_day);
         
            $user->plan_id = $plan->id;


            $user->save();
        }
        return redirect('/invoice/'.$sale->invoice_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
       
        $sale = Sale::where('invoice_id',$id)->with('user')->first();

        if($sale)
        {
            $caraPembayaran = Redis::get('caraPembayaran')
;            
            return Inertia::render('Membership/Invoice',["sale"=>$sale,"caraPembayaran"=>$caraPembayaran]);
        }else{
            return redirect('/membership');
        }
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    public function confirm($id)
    {
        //
        
        $sale = Sale::find($id);

        if($sale->status == 'berhasil' || $sale->status == 'batal')
        {
            return $sale;
        }

        $user = User::find($sale->user_id);
        
        $current_plan = Plan::find($user->plan_id);
        
        $new_plan = Plan::find($sale->plan_id);
        
        $now = Carbon::now();

        $membership_days =  $now->diffInDays($user->membership_time,false);
 
        if($membership_days > 0)
        { 

            $value = $membership_days * $current_plan->daily_price;
 

            $value_day = ceil( $value / $new_plan->daily_price);
 
            $days = ($new_plan->time_in_day+$value_day);
 
            $user->membership_time = $now->addDays($days);
         
            $user->plan_id = $new_plan->id;
 

            $user->save();

            $sale->status = 'berhasil';

            $sale->save();

        }else{

        }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }
}
