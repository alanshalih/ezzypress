<?php

namespace App\Jobs;

use App\Models\MainDomain;
use App\Models\Server;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class DeleteSite implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $timeout = 30;

    public $tries = 1;

    protected $web;

    public function __construct($web)
    {
        //
        $this->web = $web;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Redis::throttle('ploi-api-limiter')->block(0)->allow(120)->every(60)->then(function () {
            info('delete site.'); 


            $client = new Client();


            $server = Server::find($this->web->server_id); 
 

            $client->request('POST', "https://".$server->server_url."/api/drop-database", [
                'json'    => $this->web
            ]);

             $client->request('DELETE', "https://ploi.io/api/servers/{$server->server_id}/sites/{$this->web->webapp_id}", [
             
                'headers' => 
                    [
                        'Accept'        => 'application/json',
                        'Authorization' => "Bearer {$server->api_key}"
                    ]
                
            ]);

            
          
     
             

            
            DeleteSystemUser::dispatch($this->web)
            ->delay(now()->addSeconds(60));

            
            // Handle job...
        }, function () {
            // Could not obtain lock...
    
            return $this->release(60);
        });
    }
}
