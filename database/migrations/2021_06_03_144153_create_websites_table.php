<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websites', function (Blueprint $table) {
            $table->id();
            $table->string('folder'); 
            $table->string('subdomain')->nullable(); 
            $table->string('tld')->nullable();
            $table->string('reserve_domain')->unique();
            $table->string('main_domain')->index();
            $table->string('db_name')->nullable();
            $table->string('db_username')->nullable();
            $table->string('db_password')->nullable();
            $table->string('phpmyadmin_url')->nullable();
            $table->integer('db_user_id')->nullable();
            $table->bigInteger('db_id')->nullable(); 
            $table->string('ip_domain')->nullable();
            $table->foreignId('server_id')->nullable()->constrained('servers');
            $table->bigInteger('webapp_id')->nullable();  
            $table->integer('ssl_id')->nullable(); 
            $table->dateTime('ssl_valid_until')->nullable();
            $table->dateTime('ssl_renewal_at')->nullable();
            $table->boolean('is_active')->default(true);
            $table->string('website_title')->nullable();;
            $table->string('website_email')->nullable();;
            $table->string('website_username')->nullable();;
            $table->string('website_password')->nullable();;
            $table->string('system_user')->nullable();
            $table->bigInteger('total_visit')->default(0);
            $table->bigInteger('monthly_visit')->default(0);
            $table->bigInteger('daily_visit')->default(0);
            $table->bigInteger('bytes_in')->default(0);
            $table->bigInteger('bytes_out')->default(0);
            $table->bigInteger('bytes_total')->default(0);
            $table->integer('step')->default(1);
            $table->string("step_status")->nullable();
            $table->boolean('installed')->default(false);
            $table->boolean('is_archived')->default(false)->index();
            $table->string("cloudflare_dns_id")->nullable();
            $table->string("http_protocol")->default("http");
            $table->string('old_domain')->nullable();
            $table->bigInteger("certificate_id")->nullable();
            $table->string("provider")->default("ploi"); 
            $table->foreignId('user_id')->constrained('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('websites');
    }
}
