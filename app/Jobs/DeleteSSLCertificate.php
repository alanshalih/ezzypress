<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class DeleteSSLCertificate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $timeout = 30;

    public $tries = 1;


    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $web;
    public function __construct($web)
    {
        //
        $this->web = $web;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Redis::throttle('ploi-api-limiter')->block(0)->allow(120)->every(60)->then(function () {
            info('enable fast cgi...'); 

            $client = new Client();

            $server = $this->web->server; 


            
 
            if($this->web->certificate_id)
            $client->request('DELETE', "https://ploi.io/api/servers/{$server->server_id}/sites/{$this->web->webapp_id}/certificates/{$this->web->certificate_id}", [
                    
                'headers' => 
                    [
                        'Accept'        => 'application/json',
                        'Authorization' => "Bearer {$server->api_key}"
                    ]
                
            ]); 
             

            
           

            
            // Handle job...
        }, function () {
            // Could not obtain lock...
    
            return $this->release(60);
        });
    }
}
