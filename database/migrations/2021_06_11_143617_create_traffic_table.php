<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrafficTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffic', function (Blueprint $table) {
            $table->id(); 
            $table->bigInteger('website_id')->index();
            $table->bigInteger('user_id')->index();
            $table->bigInteger("request");
            $table->bigInteger('bytes_in');
            $table->bigInteger('bytes_out');
            $table->bigInteger('bytes_total');
            $table->string("domain");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traffic');
    }
}
