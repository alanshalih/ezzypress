<?php

namespace App\Jobs;

use App\Events\WebsiteInstallationStatus;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ForceHTTPs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $web;
    
    public function __construct($web)
    {
        //
        $this->web = $web;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        $client = new Client(); 

        info('force https'); 

        $config = $client->request('POST', "https://".$this->web->server_url."/api/force-https", [
            'json'    => $this->web
        ]);

        
        $this->web->http_protocol = 'https';

        $this->web->save();

        
        WebsiteInstallationStatus::dispatch($this->web->only(["installed","step","step_status","id","user_id","http_protocol"]));

        

    }
}
