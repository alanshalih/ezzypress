<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class ActivateSSL implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    public $timeout = 30;

    public $tries = 5;


    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $web;
    public function __construct($web)
    {
        //
        $this->web = $web;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Redis::throttle('ploi-api-limiter')->block(0)->allow(120)->every(60)->then(function () {
            

       

            $this->web->step_status = 'mengaktifkan SSL';

            $client = new Client();

            $server = $this->web->server; 

            $ploi = $client->request('POST', "https://ploi.io/api/servers/{$server->server_id}/sites/{$this->web->webapp_id}/certificates", [
                'json'    => ["certificate"=> $this->web->main_domain,"type"=>"letsencrypt"],
                'headers' => 
                    [
                        'Accept'        => 'application/json',
                        'Authorization' => "Bearer {$server->api_key}"
                    ]
                
            ]);

            $cert =  json_decode($ploi->getBody()); 

            $this->web->certificate_id = $cert->data->id;



             


            $this->web->save();

           
            

            
            // Handle job...
        }, function () {
            // Could not obtain lock...
    
            return $this->release(60);
        });
    }
}
