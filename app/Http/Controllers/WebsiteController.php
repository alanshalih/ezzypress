<?php

namespace App\Http\Controllers;

use App\Jobs\ActivateSSL;
use App\Jobs\AddDomainAlias;
use App\Jobs\ChangeWPDomain;
use App\Jobs\ConfigWordpress;
use App\Jobs\CreateDatabase;
use App\Jobs\CreateSite;
use App\Jobs\CreateSystemUser;
use App\Jobs\DeleteSite;
use App\Jobs\DeleteSSLCertificate;
use App\Jobs\DownloadWP;
use App\Jobs\EnableFastCGI;
use App\Jobs\FlushFastCGI;
use App\Jobs\InstallWeb;
use App\Jobs\InstallWordpress;
use App\Jobs\InstallWP;
use App\Jobs\SubmitDomain;
use App\Models\Domain;
use App\Models\MainDomain;
use App\Models\Plan;
use App\Models\Server;
use App\Models\SiteDomain;
use App\Models\User;
use App\Models\Website;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class WebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $websites = Website::where('user_id',auth()->user()->id)->latest()->get();

        if(count($websites) > 0)
        {
            return Inertia::render('Website/ListWebsite.vue',["websites"=>$websites]);
        }else{
            return Inertia::render('Dashboard');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // 
        $user = auth()->user();
        $now = Carbon::now();
        $plan = Plan::find($user->plan_id);
        if($user->membership_time < $now)
        {
            return redirect('/membership');
        }

        if(Website::where('user_id',$user->id)->count() < $plan->website_number)
        {
            $servers = Server::where('remains','>',0)->get();

            
            if(count($servers) == 0)
            {
                return view('errors.custom',["message"=>"Domain tidak tersedia","code"=>503]);
            }

            $servers->makeHidden(["api_key"]);

            return Inertia::render('Website/CreateWebsite',["servers"=>$servers]);
        }else{
            return  view('errors.custom',["message"=>"Jumlah Website yang dibuat sudah melebihi batas","code"=>503, "link"=>"/membership", "cta"=>"UPGRADE PAKET"]); ;
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        
        $data['reserve_domain'] = $data['subdomain'].'.'.$data['tld'];
        $data['main_domain'] = $data['reserve_domain'];
        
     
        $data['folder'] = str_replace(".","-",$data['reserve_domain']);
        $data['user_id'] = $request->user()->id;
        
        $server = Server::find($data['server_id']);
            
            if($server)
            {
                $server->usage = $server->usage+1;
                $server->save();
                $data['server_id'] = $server->id;
                $data['phpmyadmin_url'] = $server->db_url;
                $data['ip_domain'] = $server->domain;
                $data['provider'] = $server->provider;
                $data['server_url'] = $server->server_url;
               
            }else{
                return view('errors.custom',["message"=>"Server tidak tersedia","code"=>503]);
            }

  
         

        $web = Website::create($data);


        SiteDomain::create(["website_id"=>$web->id,"domain"=>$web->reserve_domain,"user_id"=>$request->user()->id]);


        Bus::chain([
            new CreateSystemUser($web),
            new CreateSite($web), 
           
          
       
           
        ])->dispatch();
        


        
        return redirect('/website/'.$web->id);
    }
 
 
    public function openPass($id)
    {
        $data = Website::find($id);

        if($data->user_id != auth()->user()->id){
            return ["Unauthorize action"];
        }   

        $data->makeVisible(['website_password','db_password']);

        return $data;
    } 

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Website::find($id);

        if($data->user_id != auth()->user()->id){
            return ["Unauthorize action"];
        }   
           // 
        $user = auth()->user();
        $now = Carbon::now();
        if($user->membership_time < $now)
        {
            return redirect('/membership');
        }
   
        
        $domains = SiteDomain::where('website_id',$id)->get();
        return Inertia::render('Website/ShowWebsite',["website"=>$data,"domains"=>$domains]);
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function edit(Website $website)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Website  $website
     * @return \Illuminate\Http\Response
     */

     public function changeExistedDomain(Request $request, $id)
     {
        $data = Website::find($id);

        if($data->user_id != auth()->user()->id){
            return ["Unauthorize action"];
        }   

        if($data->main_domain != $request->domain)
        {
            $data->old_domain = $data->main_domain;

            $data->main_domain = $request->domain;

            $data->save();

            ChangeWPDomain::dispatch($data);

            if($data->certificate_id)
            DeleteSSLCertificate::dispatch($data);

            if($data->main_domain != $data->reserve_domain)
            ActivateSSL::dispatch($data)->delay(now()->addSeconds(10));;

            FlushFastCGI::dispatch($data)->delay(now()->addSeconds(20));;

            return 'OK';
        }

     }
    public function changeDomain(Request $request, $id)
    {
        //
        $data = Website::find($id);

        if($data->user_id != auth()->user()->id){
            return ["Unauthorize action"];
        }   
 
        if(!$data)
        {
            return ["data website tidak ditemukan"];
        }
        if($data->main_domain != $request->domain)
        {
            $result = dns_get_record($request->domain,DNS_CNAME);
            if($result)
            {
                if($result[0]['target'] == $data->reserve_domain)
                {
                    // proses update

                    $data->old_domain = $data->main_domain;

                    $data->main_domain = $request->domain;
                    
                    // add domain
                    // /servers/{serverId}/webapps/{webAppId}/domains

                    $client = new Client();

                    
                    $domain = SiteDomain::whereDomain($request->domain)->first();

                    if($domain)
                    {
                        return ["Domain sudah digunakan"];
                    }


                    // submit to domain
                    $domain = SiteDomain::create(["website_id"=>$data->id,"domain"=>$request->domain,"user_id"=>auth()->user()->id]);


                    // submit domain alias
                   
                    $data->save();


                    AddDomainAlias::dispatch($data);
 

                    


                    return 'OK';
                }else{
                    return ["data CNAME tidak sesuai, ganti value CNAME ke ".$data->reserve_domain];
                }
            }else{
                return ["Error pengambilan data CNAME"];
            }
            
        }else{
            return ["domain website tidak ada perubahan"];
        }

        
    }

    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $data = Website::find($id);

        if($data->user_id != auth()->user()->id){
            return ["Unauthorize action"];
        }   

        $query = DB::table("servers")->where('id',$data->server_id);

        $query->decrement("usage");
        $query->increment("remains");
        
        DeleteSite::dispatch(json_decode(json_encode($data)));



        $data->delete();

        return redirect('/dashboard');

    }


    
    public function ResetSystemUser($id){
        $web = Website::find($id);

        $web->makeVisible(['system_user_pass']);

        $client = new Client();

        $server = Server::find($web->server_id);

        $web->system_user_pass = md5($web->id.'-'.now().rand(0,100000000));

        $web->system_user_password_expired_at = now()->addDay();

        $web->save();

        $client->request('POST', "https://".$server->server_url."/api/change-system-user-password", [
            'json'    => $web
        ]);

        return $web;
    }

    public function ShowSystemUser($id){
        $web = Website::find($id);

        $web->makeVisible(['system_user_pass']);

        $client = new Client();

        
        if($web->system_web_password_expired_at)
        {   
          
            
            if(now() > $web->system_user_password_expired_at)
            {
                $web->system_user_pass = md5($web->id.'-'.now().rand(0,100000000));

                $web->system_user_password_expired_at = now()->addDay();

                $web->save();

                $client->request('POST', "https://".$web->server_url."/api/change-system-user-password", [
                    'json'    => $web
                ]);

            

                return $web;

            }else{

             
               
                return $web;
            }
           
        }else{
            $web->system_user_pass = md5($web->id.'-'.now().rand(0,100000000)); 

            $web->system_user_password_expired_at = now()->addDay();

            $web->save();

            $client->request('POST', "https://".$web->server_url."/api/change-system-user-password", [
                'json'    => $web
            ]);
 

            return $web;
        }
    }

}
