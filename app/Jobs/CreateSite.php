<?php

namespace App\Jobs;

use App\Events\WebsiteInstallationStatus;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class CreateSite implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
   
    public $timeout = 30;

    public $tries = 5;


    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $web;
    public function __construct($web)
    {
        //
        $this->web = $web;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
      
 

        $server = $this->web->server;

       if($server->provider == 'ploi')
       {
            Redis::throttle('ploi-api-limiter')->block(0)->allow(120)->every(60)->then(function ()  {
               
                $client = new Client();

                $server = $this->web->server;

                $this->web->step_status = 'membuat site'; 

                $ploi = $client->request('POST', "https://ploi.io/api/servers/{$server->server_id}/sites", [
                    'json'    => ["root_domain"=>$this->web->reserve_domain,"web_directory"=>'/public',"system_user"=>$this->web->system_user],
                    'headers' => 
                        [
                            'Accept'        => 'application/json',
                            'Authorization' => "Bearer {$server->api_key}"
                        ]
                    
                ]);
 
                $site =  json_decode($ploi->getBody()); 

         
                
                $this->web->webapp_id = $site->data->id;

                $this->web->step++;
                

               

                $this->web->step_status = 'menginstall wordpress'; 
                
                $this->web->save(); 
                 

                WebsiteInstallationStatus::dispatch($this->web->only(["installed","step","step_status","id","user_id","http_protocol"])); 

                // CreateDatabase::dispatch($this->web)->delay(now()->addSeconds(10));;

                InstallWordpress::dispatch($this->web)->delay(now()->addSeconds(15));
              
        
                // Handle job...
            }, function () {
                // Could not obtain lock...
        
                return $this->release(60);
            });
       }
    }
}
