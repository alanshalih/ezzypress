<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string("plan_id")->nullable()->unique();
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('call_to_action_text')->default('Beli Paket ini');
            $table->text('point_includes')->nullable();
            $table->boolean('available_in_checkout');
            $table->boolean('auto_backup')->default(false);
            $table->boolean('is_renewable')->default(true);
            $table->boolean('active_plan')->default(true)->index();
            $table->integer('time');
            $table->integer('time_in_day');
            $table->integer('time_in_month');
            $table->string('time_unit');
            $table->integer('website_number');
            $table->bigInteger('max_monthly_traffic');
            $table->bigInteger('max_hourly_traffic'); 
            $table->bigInteger('max_monthly_transfer');
            $table->bigInteger('max_hourly_transfer'); 
            $table->integer("price");
            $table->integer("fake_price")->nullable();
            $table->integer("daily_price");
            $table->integer("monthly_price"); 
            $table->boolean("default_plan")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
