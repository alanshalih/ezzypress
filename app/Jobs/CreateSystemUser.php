<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class CreateSystemUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    
    public $timeout = 30;

    public $tries = 5;


    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $web;
    public function __construct($web)
    {
        //
        $this->web = $web;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
 

    public function handle()
    {
        //
        
        Redis::throttle('ploi-api-limiter')->block(0)->allow(120)->every(60)->then(function () {
            info('create user...');  

            $client = new Client();

            $user = $this->web->user; 

            $server = $this->web->server;

            $this->web->system_user =  $this->web->subdomain.'_'.substr(md5(rand(0,1000000000).now()),0,5);
 


            $ploi = $client->request('POST', "https://ploi.io/api/servers/{$server->server_id}/system-users", [
                'json'    => ["name"=>$this->web->system_user,"sudo"=>false],
                'headers' => 
                    [
                        'Accept'        => 'application/json',
                        'Authorization' => "Bearer {$server->api_key}"
                    ]
                
            ]);


            $usr =  json_decode($ploi->getBody());
            
            $this->web->system_user_id = $usr->data->id;

            
            $this->web->save();
        
        }, function () {
            // Could not obtain lock...
    
            return $this->release(60);
        });
            // Handle job...
       
    }
}
