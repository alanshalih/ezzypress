<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class EnableFastCGI implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

     /**
     * Create a new job instance.
     *
     * @return void
     */
    
    public $timeout = 30;

    public $tries = 5;


    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $web;
    public function __construct($web)
    {
        //
        $this->web = $web;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Redis::throttle('ploi-api-limiter')->block(0)->allow(120)->every(60)->then(function () {
            info('enable fast cgi...'); 

            $client = new Client();

            $server = $this->web->server; 

            $ploi = $client->request('POST', "https://ploi.io/api/servers/{$server->server_id}/sites/{$this->web->webapp_id}/fastcgi-cache/enable", [
             
                'headers' => 
                    [
                        'Accept'        => 'application/json',
                        'Authorization' => "Bearer {$server->api_key}"
                    ]
                
            ]);
 
             

            
            // InstallWeb::dispatch($this->web)
            // ->delay(now()->addSeconds(15));

            
            // Handle job...
        }, function () {
            // Could not obtain lock...
    
            return $this->release(60);
        });
    }
}
