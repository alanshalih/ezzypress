<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class ConfigWordpress implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $timeout = 30;

    public $tries = 5;
    
    public $backoff = 5;

    
    protected $web;

    public function __construct($web)
    {
        //
        $this->web = $web;

        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        $web = $this->web;
        
        $web->makeVisible(['website_password','db_password']);

        
        $client = new Client();

        $server = $this->web->server;
      

        $client->request('POST', "https://".$server->domain."/api/config-wordpress", [
            'json'    => $web
        ]);

//         https://dua.ezzypress.online/wp-admin/install.php?step=2

        // weblog_title: JAKARTA
        // user_name: admin
        // admin_password: !*1hZfQ8F$X5UYZ&(1
        // admin_password2: !*1hZfQ8F$X5UYZ&(1
        // admin_email: maulanashalihin@gmail.com
        // Submit: Instal WordPress
        // language: id_ID

        $client->request('POST', "https://".$server->domain."/api/config-wordpress", [
            'json'    => $web
        ]);

        // InstallWordpress::dispatch($this->web)->delay(now()->addSeconds(15)); 
            InstallWeb::dispatch($this->web)->delay(now()->addSeconds(5));

         
    }
}
