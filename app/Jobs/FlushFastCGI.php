<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class FlushFastCGI implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */ 

    public $web;
    
    public function __construct($web)
    {
        // 

        $this->web = $web;


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Redis::throttle('ploi-api-limiter')->block(0)->allow(120)->every(60)->then(function () {

        $client = new Client();

        $server = $this->web->server; 

        $ploi = $client->request('POST', "https://ploi.io/api/servers/{$server->server_id}/sites/{$this->web->webapp_id}/fastcgi-cache/flush", [
          
            'headers' => 
                [
                    'Accept'        => 'application/json',
                    'Authorization' => "Bearer {$server->api_key}"
                ]
            
        ]);

    }, function () {
        // Could not obtain lock...

        return $this->release(60);
    });



        

    }
}
