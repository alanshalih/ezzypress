<?php

namespace App\Http\Controllers;

use App\Models\MainDomain;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MainDomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $domains = MainDomain::get();
        return Inertia::render('MainDomain/Index',["domains"=>$domains]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return Inertia::render('MainDomain/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        MainDomain::create($request->all());
        return redirect('/domain');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MainDomain  $mainDomain
     * @return \Illuminate\Http\Response
     */
    public function show(MainDomain $mainDomain)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MainDomain  $mainDomain
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return Inertia::render('MainDomain/Create',["domain"=>MainDomain::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MainDomain  $mainDomain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        MainDomain::where('id',$id)->update($request->except(['id','created_at','updated_at']));
        return redirect('/domain');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MainDomain  $mainDomain
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainDomain $mainDomain)
    {
        //
    }
}
